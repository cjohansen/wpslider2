
<script type='text/javascript'>
	jQuery( document ).ready( function( $ ) {
		if ( typeof wp.media != 'undefined' ) { 
			var file_frame;
			var wp_media_post_id = wp.media.model.settings.post.id; 
			var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; 
			jQuery('#upload_image_button').on('click', function( event ){
				event.preventDefault();
				if ( file_frame ) { 
					file_frame.uploader.uploader.param( 'post_id', set_to_post_id ); 
					file_frame.open(); 
					return;
				} else {
					wp.media.model.settings.post.id = set_to_post_id;
				}
				file_frame = wp.media.frames.file_frame = wp.media({ 
					title: 'Select images to upload',
					button: {
						text: 'Use this image',
					},
					multiple: true	
				});
				file_frame.on( 'select', function() { 
					attachment = file_frame.state().get('selection').first().toJSON();
					var images = file_frame.state().get('selection').toJSON();
					addPicked(images);
					jQuery( '#image_attachment_id' ).val( attachment.id );
					wp.media.model.settings.post.id = wp_media_post_id; 
				});
					file_frame.open(); 
			});
		}
	});

</script>

