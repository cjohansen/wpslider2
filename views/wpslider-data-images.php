<?php

print "<p>Images in the slider shown in order. Right click to add tekst to image.</p>";
print "<div id='ordered_list'>";

$result = WPSlider::$db->get_results(
	"select * from ".WPSlider::$db->prefix."wpslider_images where post_id=".$data->ID
);
$ordered_images = "";
foreach ( $result as $v ) {
	$image_id = $v->image_id;
	$image_text = $v->text;
	require("wpslider-data-images-single.php");
	$ordered_images .= $image_id.",";
}
print "</div>";

print "<p>Click image below to add it to slider. </p>";
print "<div class='wrap' id='all_images'>";

$result = WPSlider::$db->get_results(
	"select * from ".WPSlider::$db->prefix."posts ".
	"where post_type='attachment' ".
	"and post_mime_type like 'image%' ".
	"and post_status='inherit' ".
	"and ID not in ".
	"( select image_id from wp5_wpslider_images where post_id=".$data->ID." )"
);

foreach ( $result as $image ) {
	$image_id = $image->ID;
	require("wpslider-data-images-single.php");
}
print "</div>";

print "<input type='text' id='ordered_images' name='ordered_images' style='display:none;' value=".$ordered_images.">";

?>

<script>
function pickImage(id) {
	var parent = jQuery('#image_'+id).parent().attr('id');
	var sw = { "all_images":"#ordered_list", "ordered_list":"#all_images" };
	jQuery(sw[parent]).append( jQuery("#image_"+id) );
	var output = '';
	jQuery("#ordered_list span").each(function(index,element) {
		var image = jQuery(element).children("img");
		output += element.id.split("_")[1]+",";
	});
	jQuery("#ordered_images").attr("value",output);
}

function addInfo(id) {
	event.preventDefault();
	var text = prompt( "Write text for image:", jQuery("#text_"+id).attr("value") );
	jQuery("#text_"+id).attr("value",text);
}
</script>

