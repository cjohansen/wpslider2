<?php
WPSlider::$id++;

$result = WPSlider::$db->get_row(
	"select * from ".WPSlider::$db->prefix."posts ".
	"where post_type='wpslider' ".
	"and post_status='publish' ".
	"and post_title='".$post_title."'"
	);
$post_id = $result->ID;
$width = get_post_meta($post_id, "width",true);
$height = get_post_meta($post_id, "height",true);
$duration = get_post_meta($post_id, "duration",true);
?>

<script src="<?=WPSlider::$url?>/assets/js/jquery.slides.min.js"></script>

<script>
	jQuery(function(){
		jQuery("#slides_<?=WPSlider::$id?>").slidesjs({
			width: <?=$width?>,
			height: <?=$height?>,
			navigation: {
				active: false,
			},
			pagination: {
				active: false,
			},
			play: {
				interval: <?=($duration*1000)?>,
				auto: true
			}

		});
	});

	function linkPage() {
		alert("videre til page");
	}
</script>

<div id="slides_<?=WPSlider::$id?>" style="width:<?=$width?>px; height:<?=$height?>px;">
	<?php
	$result = WPSlider::$db->get_results(
		"select * from ".WPSlider::$db->prefix."wpslider_images where post_id=".$post_id
		);
	foreach ( $result as $v ) {
		print "<img src='".$v->url."' onclick='linkPage()'>";
	}
	?>
</div>
