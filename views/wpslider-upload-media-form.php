<?php

print "Click image to change order";

wp_enqueue_media();

$result = WPSlider::$db->get_results(
	"select * from ".WPSlider::$db->prefix."wpslider_images where post_id=".$data->ID
);
$ordered_images = "[";
foreach ( $result as $v ) {
	$ordered_images .= "{'url':'".$v->url."'},";
}
$ordered_images = substr($ordered_images,0,-1);
$ordered_images .= "]";

?>

<script>

	function addPicked(images) {
		for ( i in images ) {
			var gid = (new Date()).getMilliseconds()*100+i;
			jQuery("#billede").append(
				"<div "+
					"id='picked_"+gid+"' "+
					"style='padding:3px; width:100px; height:100px; overflow:hidden; float:left;' "+
					"onclick='pickImage("+gid+")' "+
				">"+
					"<img src='"+images[i].url+"' height='100'>"+
					"<input type='hidden' name='src_"+gid+"' value='"+images[i].url+"'>"+
				"</div>"
			);
		}		
	}

	function pickImage(number) {
		var picked = jQuery('#billede div');
		var forrige = '';
		for ( i in picked ) {	
			if ( picked[i].id == 'picked_'+number ) {
				if ( i == 0 ) {
					picked[i].remove();
				}
				else {
					picked[i-1].before(picked[i]);
				}
			}
		}
	}


</script>

<form method='post'>
	<div class='image-preview-wrapper' id="billede" >
	</div>
	<div style="clear:both;"></div>
	<input id="upload_image_button" type="button" class="button" value="<?php _e( 'Add images' ); ?>" />
	<!--<input type='hidden' name='image_attachment_id' id='image_attachment_id' value='<?php echo get_option( 'media_selector_attachment_id' ); ?>'>-->
	<!--<input type="submit" name="submit_image_selector" value="Save" class="button-primary">-->
</form>

<div id="ordered_list">
</div>

<script>
	addPicked(<?php echo $ordered_images; ?>);

</script>

