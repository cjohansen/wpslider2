<?php
class WPSlider_Widget extends WP_Widget {
	public function __construct() {
		$title = "wpslider";
		$id = "wpslider_widget";
		$args = [
			"description" => "wpslider",
			"classname" => "wpslider-widget",
		];

		parent::__construct($id,$title,$args);
	}

	public function widget($args, $instance) {
		echo $args["before_widget"];

		if ( isset($instance["title"]) && !empty($instance["title"]) ) {
			echo 	$args["before_title"].
					$instance["title"].
					$args["after_title"];
		} 

		$post_title = $instance["wpslider"];
		require("views/wpslider-entry.php");

		echo $args["after_widget"];
	}

	public function form($instance) {
		$title = ( !empty($instance["title"]) ? $instance["title"] : "new title" );
		$wpslider = ( !empty($instance["wpslider"]) ? $instance["wpslider"] : "" );

		require("views/widget-entry.php");
	}

	public function update($new_instance, $old_instance) {
		$instance = [];
		$instance["title"] = $new_instance["title"];
		$instance["wpslider"] = $new_instance["wpslider"];
		return $instance;
	}
}

new WPSlider_Widget();

add_action("widgets_init", function() {
	register_widget("WPSlider_Widget");
});


