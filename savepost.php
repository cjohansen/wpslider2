<?php

add_action("save_post",function($post){

	$billeder = array_filter(array_keys($_POST), function($key) {
		if ( substr($key,0,4) == "src_" ) return true;
	});

	WPSlider::$db->delete(WPSlider::$db->prefix."wpslider_images",["post_id"=>$_POST["ID"]]);

	foreach ( $billeder as $v ) {
		WPSlider::$db->show_errors();
		$sqldata = [
			"post_id" => sanitize_text_field($_POST["ID"]),
			"url" => sanitize_text_field($_POST[$v])
		];
		$res = WPSlider::$db->insert(WPSlider::$db->prefix."WPSlider_images",$sqldata);
	}	

});

?>
