<?php
class CustomTypePost 
{
	public function __construct() {
		add_action("init",[$this,"set_cpt_wpslider"]);
		add_action("add_meta_boxes",[$this,"set_meta_boxes"]);
		add_action("save_post",[$this,"save_custom_post_type"]);	
	}

	public function set_cpt_wpslider() {
		register_post_type("wpslider",[
				"label" => "WPSlider",
				"public" => true,
				"hierarchical" => true,
				"supports" => ["title","revisions"]
			]);
	}

	public function set_meta_boxes() {
		add_meta_box(
			"wpslider_dimensions",
			"WPSlider Dimensions",
			[$this,"wpslider_data_dimensions_view"],
			"wpslider",
			"side",
			"default"
			);
		add_meta_box(
			"wpslider_upload",
			"WPSlider Images",
			[$this,"wpslider_data_upload_view"],
			"wpslider",
			"advanced",
			"default"
			);
	}

	public function wpslider_data_upload_view($data) {
		require_once("views/wpslider-upload-media-form.php");
	}

	public function wpslider_data_dimensions_view($data) {
		$width = get_post_meta($data->ID, "width",true);
		$height = get_post_meta($data->ID, "height",true);
		$duration = get_post_meta($data->ID, "duration",true);
		require_once("views/wpslider-data-dimensions.php");
	}

	public function wpslider_data_images_view($data) {
		require_once("views/wpslider-data-images.php");
	}

	public function save_custom_post_type($post_id) {
		if (isset($_POST["width"]) && !empty($_POST["width"]) ) {
			update_post_meta($post_id,"width",$_POST["width"]);
		}
		if (isset($_POST["height"]) && !empty($_POST["height"]) ) {
			update_post_meta($post_id,"height",$_POST["height"]);
		}
		if (isset($_POST["duration"]) && !empty($_POST["duration"]) ) {
			update_post_meta($post_id,"duration",$_POST["duration"]);
		}

		$billeder = array_filter(array_keys($_POST), function($key) {
			if ( substr($key,0,4) == "src_" ) return true;
		});

		WPSlider::$db->delete(WPSlider::$db->prefix."wpslider_images",["post_id"=>$_POST["ID"]]);

		foreach ( $billeder as $v ) {
			WPSlider::$db->show_errors();
			$sqldata = [
				"post_id" => intval($_POST["ID"]),
				"url" => sanitize_text_field($_POST[$v])
			];
			$res = WPSlider::$db->insert(WPSlider::$db->prefix."WPSlider_images",$sqldata);
		}	
	} 
}

new CustomTypePost();
?>