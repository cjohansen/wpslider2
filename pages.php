<?php


class Pages {

	public function __construct() {
		add_action("admin_enqueue_scripts",[$this,"styles"]);
		add_action("wp_enqueue_scripts",[$this,"styles"]);
	}

	public function styles() {
		$this->set_styles();
		$this->set_scripts();
	}

	private function set_styles() {
		wp_enqueue_style(	"test-main",
							WPSlider::$url . "/assets/css/main.css",
							[],
							null,
							"screen");
	}

	private function set_scripts() {
		wp_enqueue_script(	"script-main",
							"http://code.jquery.com/jquery-latest.min.js",
							[],
							null,
							false);
	}
}

new Pages();





